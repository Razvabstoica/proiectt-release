
1. Uvod

Ova Licenca primenjuje se na racunarski program sa kojim se distribuira (od sada " Program "). Program je intelektualno vlasni�tvo Andrei Timisescu (" Autor ") i sme�ten je pod za�titu zakona o autorskim pravima.

Molim Vas uocite da ovo nije besplatna softverska licenca i nije definisana od Free Software Foundation-a. Upotreba softvera je pokrivena ovom licencom, moze se koristiti besplatno, ali njegova KOMERCIJALNA DISTRIBUCIJA JE ZABRANJENA. Dok autor ovog softvera podr�ava filozofiju otvorenog koda on takodje distribuira drugi GPL-ovla�teni softver, ova licenca znaci da nikakva zarada nece moci biti napravljena od third parties programa od ovog softvera.

PRI KORI�TENJU ILI DISTRIBUCIJI PROGRAMA (ILI BILO KOG POSLA TEMELJENOG NA PROGRAMU), VI PRIHVATATE OVU LICENCU, I SVE NJEGOVE USLOVE I STANJA ZA KORI�TENJE I DISTRIBUCIJU PROGRAMA ILI RADA TEMELJENOG NA NJEMU. NI�TA OSIM OVE LICENCE NE DOPU�TA VAM DA DELITE PROGRAM ILI NJEGOVE IZVEDENE RADNJE. OVE AKCIJE SU ZABRANJENE ZAKONOM. AKO NE PRIHVATITE OVE ZAHTEVE I STANJA, NE DISTRIBUIRAJTE PROGRAM.

2, Licenca

Izdavac licence dodeljuje vam sledeca prava, pod uslovom da se sla�ete sa svim ogranicenjima ove Licence i unapred vam daje pravo da vi distribuirate nepromijenjenu kopiju ove Licence s Programom:

   1. Dopu�teno je da upotrebljavate Programu za bilo koje svrhe.
   2. Vi mo�ete kopirati i distribuirati doslovnu kopiju Programa kroz svijet, u bilo kojem mediju.
   3. Vi mo�ete stvoriti radove temeljene na Programu i distribuirati kopije takvog kroz svijet, u bilo kojem mediju.

3.Ogranicenja

Ova licenca je podlozna sledecim ogranicenjima:
   1. Svaki distributer ovog Programa mora zadr�ati kopiju ove Licence.
   2. Program nece moci biti prodan ili pripojen  u bilo koji proizvod koji je prodan bez prethodnog dopu�tenja od Autora. Distribucija Programa ili bilo koji posao temeljen na Programu kod komercijalne organizacije prema bilo kojoj trecoj strani bez dopu�tzanja je zabranjen, eventualno placanje ucinjeno u vezi sa takvom distribucijom, da li direktno (kao u placanju za kopiju Programa) ili indirektno (kao u placanju servisa vezanog za Program, ili placanje za neki proizvod ili usluga koja ukljuci kopiju Programa " besplatno ").
   3. Bilo sta sto je proizvedeno pomocu Programa, ili izvedeno iz njega, je podlo�noj ovoj licenci, to jest,  sadr�aj napravljen radom Programa bi mogao biti slobodno deljen pod uslovom da ni direktno ni indirektno placanje nije potrebno kod distributera u zamjenu za ovaj sadr�aj. Distribucija proizvedenog sadr�aja mora takode slediti pravila koja ne bi li mogla upravljati distribucijom i eksploatacija ulaznog materijala i/ili softvera koji je generisan.

4. Rezervacija Prava

Nikakva prava nisu dana Programu izuzimajuci ako se izricito ovdje izloze. Vi ne mo�ete kopirati, menjati, podlicense, ili distribuirati Program sem ako nije izricito ovde to naznaceno. Bilo koji poku�aj kopiranja, modifikacije, podlicenciranja ili distribucije Programa je zabranjeno, i automatski ce prekinuti va�a prava na Licencu. Medutim, onaj ko primi kopije, ili prava, od vas pod ovom licencom nece je imati tako dugo kao oni koji imaju u potpunu licencu.

5. Ogranicenja

SOFTVER JE DOSTAVLJEN " KAKO JE ", BEZ GARANCIJE BILO KOJE VRSTE, EKSPRES ILI JE IMPLICIRAO, UKLJUCUJUCI ALI NE OGRANICAVAJUCI GARANCIJE MOGUCNOSTI PRODAJE, SPOSOBNOST ZA ODRE�ENU SVRHU I NEKRSENJE. NI U JEDNOM DOGA�AJU AUTORI ILI VLASNIK AUTORSKIH PRAVA NECE BITI ODGOVORAN ZA BILO KOJI TU�BENI ZAHTEV, ODSTETU ILI IMATI DRUGU ODGOVORNOST, DA LI U AKCIJI UGOVARANJA, DELIKT ILI INACE, PROIZICI IZ, IZVAN ILI U VEZI SA SOFTVEROM ILI UPOTREBA ILI DRUGA POSLOVANJA U SOFTVERU.