Version:5.2.5
Toc 3cam cu garnitura|3cam|toate|alb|0|0|0|0|0|0|0
To3gt|0|Arm25x30x25|0|
Z fer 3cam cu garnitura|3cam|toate|alb|0|0|0|0|0|0|2
Z3g|0|Arm25x30x25|0|
Teu 3cam cu garnitura|3cam|toate|alb|0|0|0|0|0|0|1
T3g|0|CPT3|2|SR|2|Arm28x25x28|0|
Z intermediar|3cam|toate|alb|0|0|0|0|0|0|3
Zint |0|Arm40x30x40|0|
Montant mobil 3cam cu garnitura|3cam|toate|alb|0|0|0|0|0|0|4
Mog|0|Arm25x30x25|0|CM3-4|2|
Toc 4cam cu garnitura|4cam cu garnitura|toate|alb|0|0|0|0|0|0|0
To4g|0|Arm26x33x26|0|
Z fer 4cam cu garnitura|4cam cu garnitura|toate|alb|0|0|0|0|0|0|2
Zf4g|0|Arm26x33x26|0|
Teu 4cam cu garnitura|4cam cu garnitura|toate|alb|0|0|0|0|0|0|1
T4g|0|CPT4|2|SR|2|Arm28x25x28|0|
Z usa 4cam cu garnitura|4cam cu garnitura|toate|alb|0|0|0|0|0|0|3
Zu4g|0|Arm50x30x50|0|
T usa 4cam cu garnitura|4cam cu garnitura|toate|alb|0|0|0|0|0|0|3
Tu4g|0|Arm50x30x50|0|
Montant mobil 4cam cu garnitura|4cam cu garnitura|toate|alb|0|0|0|0|0|0|4
CM3-4|2|Arm25x30x25|0|Mog|0|
Toc 4cam|4cam|toate|alb|0|0|0|0|0|0|0
To4|0|Arm26x33x26|0|Grs|0|
Z fer 4cam|4cam|toate|alb|0|0|0|0|0|0|2
Zf4|0|Gre|0|Arm26x33x26|0|Grs|0|
Teu 4cam|4cam|toate|alb|0|0|0|0|0|0|1
T4|0|Grs|0|Grs|0|Arm28x25x28|0|CPT4|2|SR|2|
Z usa 4cam|4cam|toate|alb|0|0|0|0|0|0|3
Zu4|0|Arm50x30x50|0|Grs|0|Gre|0|
T usa 4cam|4cam|toate|alb|0|0|0|0|0|0|3
Tu4|0|Arm50x30x50|0|Grs|0|Gre|0|
Montant mobil 4cam|4cam|toate|alb|0|0|0|0|0|0|4
Mo|0|Arm25x30x25|0|CM3-4|2|Grs|0|
Toc 5cam cu garnitura|5cam|toate|alb|0|0|0|0|0|0|0
To5|0|Arm26x33x26|0|
Z fer 5cam cu garnitura|5cam|toate|alb|0|0|0|0|0|0|2
Z5|0|Arm26x33x26|0|
Teu 5cam cu garnitura|5cam|toate|alb|0|0|0|0|0|0|1
T5|0|CPT5|2|Arm28x25x28|0|SR|2|
Z usa 5cam cu garnitura|5cam|toate|alb|0|0|0|0|0|0|3
Zu5|0|Arm50x30x50|0|
T usa 5cam cu garnitura|5cam|toate|alb|0|0|0|0|0|0|3
Tu5|0|Arm50x30x50|0|
Montant mobil 5cam cu garnitura|5cam|toate|alb|0|0|0|0|0|0|4
Mo5|0|Arm25x30x25|0|CM5|2|
Toc 4cam laminat|4cam cu garnitura|toate|laminat|0|0|0|0|0|0|0
LTo4|0|Arm26x33x26|0|
Z fer 4cam laminat|4cam cu garnitura|toate|laminat|0|0|0|0|0|0|2
LZ4|0|Arm26x33x26|0|
Teu 4cam laminat|4cam cu garnitura|toate|laminat|0|0|0|0|0|0|1
LT4|0|Arm28x25x28|0|CPT4|2|SR|2|
Z usa 4cam laminat|4cam cu garnitura|toate|laminat|0|0|0|0|0|0|3
Arm50x30x50|0|LZu4|0|
T usa 4cam laminat|4cam cu garnitura|toate|laminat|0|0|0|0|0|0|3
LTu4|0|Arm50x30x50|0|
Montant mobil 4cam laminat|4cam cu garnitura|toate|laminat|0|0|0|0|0|0|4
LMo4|0|Arm25x30x25|0|CM3-4|2|
Toc 5cam laminat|5cam|toate|laminat|0|0|0|0|0|0|0
LTo5|0|Arm26x33x26|0|
Z fer 5cam laminat|5cam|toate|laminat|0|0|0|0|0|0|2
LZu5|0|Arm26x33x26|0|
Teu 5cam laminat|5cam|toate|laminat|0|0|0|0|0|0|1
LT5|0|CPT5|2|SR|2|Arm28x25x28|0|
Z usa 5cam laminat|5cam|toate|laminat|0|0|0|0|0|0|3
LZu5|0|Arm50x30x50|0|
T usa 5cam laminat|5cam|toate|laminat|0|0|0|0|0|0|3
LTu5|0|Arm50x30x50|0|
Montant mobil 5cam laminat|5cam|toate|laminat|0|0|0|0|0|0|4
LMo5|0|Arm25x30x25|0|CM5|2|
Patrat de colt 3cam|3cam|toate|alb|0|0|0|0|0|0|8
Pt3|0|Arm40x40|0|
Patrat de colt 4cam|4cam|toate|alb|0|0|0|0|0|0|8
Pt3|0|Arm40x40|0|
Patrat de colt 4cam cu garnitura|4cam cu garnitura|toate|alb|0|0|0|0|0|0|8
Pt3|0|Arm40x40|0|
Patrat de colt 5cam|5cam|toate|alb|0|0|0|0|0|0|8
Pt5|0|Arm40x40|0|
Patrat de colt 4cam laminat|4cam cu garnitura|toate|laminat|0|0|0|0|0|0|8
LPt4|0|Arm40x40|0|
Patrat de colt 5cam laminat|5cam|toate|laminat|0|0|0|0|0|0|8
LPt5|0|Arm40x40|0|
H de legatura 3cam|3cam|toate|alb|0|0|0|0|0|0|8
HL|0|
H de legatura 4cam|4cam|toate|alb|0|0|0|0|0|0|8
HL|0|
H de legatura 4cam cu garnitura|4cam cu garnitura|toate|alb|0|0|0|0|0|0|8
HL|0|
H de legatura 5cam|5cam|toate|alb|0|0|0|0|0|0|8
Hl5|0|
Prag|toate|toate|toate|0|0|0|0|0|0|10
CPP|2|PR|0|
H de legatura 4cam laminat|4cam cu garnitura|toate|laminat|0|0|0|0|0|0|8
LHl|0|
H de legatura 5cam laminat|5cam|toate|laminat|0|0|0|0|0|0|8
LHl5|0|
Teava 3cam|3cam|toate|alb|0|0|0|0|0|0|8
Tv3|0|CA|0|CA|0|ArmT|0|
Teava 4cam|4cam|toate|alb|0|0|0|0|0|0|8
Tv3|0|CA|0|CA|0|ArmT|0|
Teava 4cam cu garnitura|4cam cu garnitura|toate|alb|0|0|0|0|0|0|8
Tv3|0|CA|0|CA|0|ArmT|0|
Teava 5cam|5cam|toate|alb|0|0|0|0|0|0|8
Tv5|0|CA5|0|CA5|0|ArmT|0|
Teava 4cam laminat|4cam cu garnitura|toate|laminat|0|0|0|0|0|0|8
LTv|0|LCA4|0|LCA4|0|ArmT|0|
Teava 5cam laminat|5cam|toate|laminat|0|0|0|0|0|0|8
LTv5|0|LCA5|0|LCA5|0|ArmT|0|
Ghidaj Lateral Rulou - Alb|ALUSEL|toate|alb|0|0|0|0|0|0|14
RLP11|0|RLPGP|0|RLPGP|0|
Ghidaj Lateral Rulou - Laminat|ALUSEL|toate|laminat|0|0|0|0|0|0|14
RLP11c|0|RLPGP|0|RLPGP|0|
Ghidaj Lateral Rulou - Maro|ALUSEL|toate|maro|0|0|0|0|0|0|14
RLP11m|0|RLPGP|0|RLPGP|0|
Ghidaj Central (T)  Rulou - Alb|ALUSEL|toate|alb|0|0|0|0|0|0|-1
RLP11|0|RLP11|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|
Ghidaj Central (T)  Rulou - Laminat|ALUSEL|toate|laminat|0|0|0|0|0|0|-1
RLP11c|0|RLP11c|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|
Ghidaj Central (T)  Rulou - Maro|ALUSEL|toate|maro|0|0|0|0|0|0|-1
RLP11m|0|RLP11m|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|
Lamele Rulou - Alb|ALUSEL|toate|alb|-75|0|0|0|0|0|15
RLP3|0|RLPA3|1|
Lamele Rulou - Laminat|ALUSEL|toate|laminat|-75|0|0|0|0|0|15
RLP3c|0|RLPA3c|1|
Lamele Rulou - Maro|ALUSEL|toate|maro|-75|0|0|0|0|0|15
RLP3m|0|RLPA3c|1|
Caseta Superioara - Rulou Alb - 137|ALUSEL|toate|alb|-15|0|200|4000|200|1390|18
RLP1-137|0|
Caseta Superioara - Rulou Laminat - 137|ALUSEL|toate|laminat|-15|0|200|4000|200|1390|18
RLP1-137c|0|
Caseta Superioara - Rulou Maro - 137|ALUSEL|toate|maro|-15|0|200|4000|200|1390|18
RLP1-137m|0|
Caseta Superioara - Rulou Alb - 165|ALUSEL|toate|alb|-15|0|200|4000|1391|2390|18
RLP1-165|0|
Caseta Superioara - Rulou Laminat - 165|ALUSEL|toate|laminat|-15|0|200|4000|1391|2390|18
RLP1-165c|0|
Caseta Superioara - Rulou Maro - 165|ALUSEL|toate|maro|-15|0|200|4000|1391|2390|18
RLP1-165m|0|
Caseta Superioara - Rulou Alb - 180|ALUSEL|toate|alb|-15|0|200|4000|2391|2600|18
RLP1-180|0|
Caseta Superioara - Rulou Laminat - 180|ALUSEL|toate|laminat|-15|0|200|4000|2391|2600|18
RLP1-180c|0|
Caseta Superioara - Rulou Maro - 180|ALUSEL|toate|maro|-15|0|200|4000|2391|2600|18
RLP1-180m|0|
Caseta Superioara - Rulou Alb 180 - (h>2600) |ALUSEL|toate|alb|-15|0|200|4000|2601|4000|18
RLP1-180|0|
Caseta Superioara - Rulou Laminat 180 - (h>2600) |ALUSEL|toate|laminat|-15|0|200|4000|2601|4000|18
RLP1-180c|0|
Caseta Superioara - Rulou Maro 180 - (h>2600) |ALUSEL|toate|maro|-15|0|200|4000|2601|4000|18
RLP1-180m|0|
Lamela terminala - Rulou - Alb|ALUSEL|toate|alb|-75|0|0|0|0|0|16
RLP4|0|RLPGT|0|RLPA8|2|
Lamela terminala - Rulou - Laminat|ALUSEL|toate|laminat|-75|0|0|0|0|0|16
RLP4c|0|RLPGT|0|RLPA8c|2|
Lamela terminala - Rulou - Maro|ALUSEL|toate|maro|-75|0|0|0|0|0|16
RLP4m|0|RLPGT|0|RLPA8c|2|
Ghidaj Lateral Usa Garaj - Alb|ALUSEL-Usa Garaj|toate|alb|0|0|0|0|0|0|14
RLP11G|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|
Ghidaj Lateral Usa Garaj - Laminat|ALUSEL-Usa Garaj|toate|laminat|0|0|0|0|0|0|14
RLP11Gc|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|
Ghidaj Lateral Usa Garaj - Maro|ALUSEL-Usa Garaj|toate|maro|0|0|0|0|0|0|14
RLP11Gm|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|
Ghidaj Central (T) Usa Garaj - Alb|ALUSEL-Usa Garaj|toate|alb|0|0|0|0|0|0|-1
RLP11G|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|RLP11G|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|
Ghidaj Central (T) Usa Garaj - Laminat|ALUSEL-Usa Garaj|toate|laminat|0|0|0|0|0|0|-1
RLP11Gc|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|RLP11Gc|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|
Ghidaj Central (T) Usa Garaj - Maro|ALUSEL-Usa Garaj|toate|maro|0|0|0|0|0|0|-1
RLP11Gm|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|RLP11Gm|0|RLPGP|0|RLPGP|0|RLPGP|0|RLPGP|0|
Lamele Usa Garaj  - Alb|ALUSEL-Usa Garaj|toate|alb|-120|0|0|0|0|0|15
RLP3G|0|RLPA3G|1|
Lamele Usa Garaj  - Laminat|ALUSEL-Usa Garaj|toate|laminat|-120|0|0|0|0|0|15
RLP3Gc|0|RLPA3Gc|1|
Lamele Usa Garaj  - Maro|ALUSEL-Usa Garaj|toate|maro|-120|0|0|0|0|0|15
RLP3Gm|0|RLPA3Gc|1|
Caseta Superioara - Usa Garaj- Alb - 250|ALUSEL-Usa Garaj|toate|alb|-10|0|1701|4700|1700|2600|18
RLP1-250G|0|
Caseta Superioara - Usa Garaj- Laminat - 250|ALUSEL-Usa Garaj|toate|laminat|-10|0|1701|4700|1700|2600|18
RLP1-250Gc|0|
Caseta Superioara - Usa Garaj- Maro - 250|ALUSEL-Usa Garaj|toate|maro|-10|0|1701|4700|1700|2600|18
RLP1-250Gm|0|
Caseta Superioara - Usa Garaj- Alb - 300|ALUSEL-Usa Garaj|toate|alb|-10|0|1701|4700|2601|3600|18
RLP1-300G|0|
Caseta Superioara - Usa Garaj- Laminat - 300|ALUSEL-Usa Garaj|toate|laminat|-10|0|1701|4700|2601|3600|18
RLP1-300Gc|0|
Caseta Superioara - Usa Garaj- Maro - 300|ALUSEL-Usa Garaj|toate|maro|-10|0|1701|4700|2601|3600|18
RLP1-300Gm|0|
Caseta Superioara - Usa Garaj- Alb - 350|ALUSEL-Usa Garaj|toate|alb|-10|0|1701|4700|3601|4700|18
RLP1-350G|0|
Caseta Superioara - Usa Garaj- Laminat - 350|ALUSEL-Usa Garaj|toate|laminat|-10|0|1701|4700|3601|4700|18
RLP1-350Gc|0|
Caseta Superioara - Usa Garaj- Maro - 350|ALUSEL-Usa Garaj|toate|maro|-10|0|1701|4700|3601|4700|18
RLP1-350Gm|0|
Lamela terminala - Usa garaj - Alb|ALUSEL-Usa Garaj|toate|alb|-120|0|0|0|0|0|16
RLP4G|0|RLPGT-77|0|
Lamela terminala - Usa garaj - Laminat|ALUSEL-Usa Garaj|toate|laminat|-120|0|0|0|0|0|16
RLP4Gc|0|RLPGT-77|0|
Lamela terminala - Usa garaj - Maro|ALUSEL-Usa Garaj|toate|maro|-120|0|0|0|0|0|16
RLP4Gm|0|RLPGT-77|0|
