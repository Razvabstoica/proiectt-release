Version:5.0
Toc (3 camere)|3 camere|toate|alb|0|0|0|0|0|0|0
OT3|0|23X28X23|0|GRN2|0|
Toc cu garnitura (3 camere)|3 camere cu garnitura|toate|alb|0|0|0|0|0|0|0
OT3G|0|23X28X23|0|
Toc stejar (3 camere)|3 camere|toate|stejar|0|0|0|0|0|0|0
OT3S|0|23X28X23|0|GRN2|0|
Toc mahon (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|0
OT3M|0|23X28X23|0|GRN2|0|
Toc cu garnitura (4 camere)|4 camere cu garnitura|toate|alb|0|0|0|0|0|0|0
OT4G|0|23X28X23|0|
Toc mare cu garnitura (4 camere)|4 camere cu garnitura|toate|alb|0|0|0|0|0|0|0
OTM4G|0|23X28X23|0|
Toc cu garnitura (5 camere)|5 camere cu garnitura|toate|alb|0|0|0|0|0|0|0
OT5G|0|23X28X23|0|
Toc mahon (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|0
OT5M|0|23X28X23|0|GRN2|0|
Toc stejar (5 camere)|5 camere|toate|stejar|0|0|0|0|0|0|0
OT5S|0|23X28X23|0|GRN2|0|
Toc glisare|glisare|toate|alb|0|0|0|0|0|0|5
OTGL|0|20X45X20|0|
Toc glisare stejar|glisare|toate|stejar|0|0|0|0|0|0|5
OTGLS|0|20X45X20|0|
T (3 camere)|3 camere|toate|alb|0|0|0|0|0|0|1
OTE3|0|23X28X23|0|CPT|2|GRN2|0|GRN2|0|
T cu garnitura (3 camere)|3 camere cu garnitura|toate|alb|0|0|0|0|0|0|1
OTE3G|0|23X28X23|0|CPT|2|
T mahon (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|1
OTE3M|0|23X28X23|0|GRN2|0|GRN2|0|CPT|2|
T stejar (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|1
OTE3S|0|23X28X23|0|GRN2|0|GRN2|0|CPT|2|
T cu garnitura (4 camere)|4 camere cu garnitura|toate|alb|0|0|0|0|0|0|1
OTE4G|0|23X28X23|0|CPT|2|
T cu garnitura (5 camere)|5 camere cu garnitura|toate|alb|0|0|0|0|0|0|1
OTF5G|0|23X28X23|0|CPT|2|
T  mahon (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|1
OTE5M|0|23X28X23|0|GRN2|0|GRN2|0|CPT|2|
T  stejar (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|1
OTE5S|0|23X28X23|0|GRN2|0|GRN2|0|CPT|2|
T usa (3 camere)|3 camere|toate|alb|0|0|0|0|0|0|3
OTU3|0|50X30X50|0|GRN1|0|GRN2|0|
T usa cu garnitura (3 camere)|3 camere cu garnitura|toate|alb|0|0|0|0|0|0|3
OTU3G|0|50X30X50|0|
T usa mahon (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|3
OTU3M|0|50X30X50|0|GRN1|0|GRN2|0|
T usa stejar (3 camere)|3 camere|toate|stejar|0|0|0|0|0|0|3
OTU3S|0|50X30X50|0|GRN1|0|GRN2|0|
T usa cu garnitura (4 camere)|4 camere cu garnitura|toate|alb|0|0|0|0|0|0|3
OTU3G|0|50X30X50|0|
T usa cu garnitura (5 camere)|5 camere cu garnitura|toate|alb|0|0|0|0|0|0|3
OTU3G|0|50X30X50|0|
T usa mahon (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|3
50X30X50|0|OTU3M|0|GRN1|0|GRN2|0|
T usa stejar (5 camere)|5 camere|toate|stejar|0|0|0|0|0|0|3
50X30X50|0|OTU3S|0|GRN1|0|GRN2|0|
Z (3 camere)|3 camere|toate|alb|0|0|0|0|0|0|2
OZ3|0|23X28X23|0|GRN1|0|GRN2|0|
Z cu garnitura (3 camere)|3 camere cu garnitura|toate|alb|0|0|0|0|0|0|2
OZ3G|0|23X28X23|0|
Z mahon (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|2
OZ3M|0|23X28X23|0|GRN1|0|GRN2|0|
Z stejar (3 camere)|3 camere|toate|stejar|0|0|0|0|0|0|2
OZ3S|0|23X28X23|0|GRN1|0|GRN2|0|
Z cu garnitura (4 camere)|4 camere cu garnitura|toate|alb|0|0|0|0|0|0|2
OZ4G|0|23X28X23|0|
Z cu garnitura (5 camere)|5 camere cu garnitura|toate|alb|0|0|0|0|0|0|2
OZ5G|0|23X28X23|0|
Z  mahon (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|2
OZ5M|0|23X28X23|0|GRN1|0|GRN2|0|
Z  stejar (5 camere)|5 camere|toate|stejar|0|0|0|0|0|0|2
OZ5S|0|23X28X23|0|GRN1|0|GRN2|0|
Z usa (3 camere)|3 camere|toate|alb|0|0|0|0|0|0|3
OZU3|0|50X30X50|0|GRN1|0|GRN2|0|
Z usa cu garnitura (3 camere)|3 camere cu garnitura|toate|alb|0|0|0|0|0|0|3
OZU3G|0|50X30X50|0|
Z usa mahon (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|3
50X30X50|0|OZU3M|0|GRN2|0|GRN1|0|
Z usa stejar (3 camere)|3 camere|toate|stejar|0|0|0|0|0|0|3
50X30X50|0|OZU3S|0|GRN2|0|GRN1|0|
Z usa cu garnitura (4 camere)|4 camere cu garnitura|toate|alb|0|0|0|0|0|0|3
OZU3G|0|50X30X50|0|
Z usa cu garnitura (5 camere)|5 camere cu garnitura|toate|alb|0|0|0|0|0|0|3
OZU3G|0|50X30X50|0|
Z usa mahon (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|3
50X30X50|0|OZU3M|0|GRN1|0|GRN2|0|
Z usa stejar (5 camere)|5 camere|toate|stejar|0|0|0|0|0|0|3
50X30X50|0|OZU3S|0|GRN1|0|GRN2|0|
Z usa glisare|glisare|toate|alb|0|0|0|0|0|0|3
OZGL|0|23X28X23|0|
Z usa glisare stejar|glisare|toate|stejar|0|0|0|0|0|0|3
OZGLS|0|23X28X23|0|
Inversor alb (3 camere)|3 camere|toate|alb|0|0|0|0|0|0|4
OMM3G|0|20X20X20|0|CPM|2|
Inversor alb cu garnitura (3 camere)|3 camere cu garnitura|toate|alb|0|0|0|0|0|0|4
OMM3G|0|20X20X20|0|CPM|2|
Inversor mahon (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|4
OMM3GM|0|20X20X20|0|CPMWM|2|
Inversor stejar (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|4
OMM3GS|0|20X20X20|0|CPMWM|2|
Inversor alb (4 camere)|4 camere cu garnitura|toate|alb|0|0|0|0|0|0|4
OMM3G|0|20X20X20|0|CPM|2|
Inversor alb (5 camere)|5 camere cu garnitura|toate|alb|0|0|0|0|0|0|4
OMM3G|0|20X20X20|0|CPM|2|
Inversor mahon (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|4
OMM3GM|0|20X20X20|0|CPMWM|2|
Inversor stejar (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|4
OMM3GS|0|20X20X20|0|CPMWM|2|
Patrat de colt (3 camere)|3 camere|toate|alb|0|0|0|0|0|0|8
OPC3|0|50x50|0|
Patrat de colt mahon (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|8
OPC3M|0|50x50|0|
Patrat de colt stejar (3 camere)|3 camere|toate|stejar|0|0|0|0|0|0|8
OPC3S|0|50x50|0|
Patrat de colt (4 camere)|4 camere cu garnitura|toate|alb|0|0|0|0|0|0|8
OPC3|0|50x50|0|
Patrat de colt (5 camere)|5 camere cu garnitura|toate|alb|0|0|0|0|0|0|8
OPC5|0|50x50|0|
Patrat de colt mahon (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|8
OPC5M|0|50x50|0|
Patrat de colt stejar (5 camere)|5 camere|toate|stejar|0|0|0|0|0|0|8
OPC5S|0|50x50|0|
Teava colt  (3 camere)|3 camere|toate|alb|0|0|0|0|0|0|8
OT60|0|50|0|OCA60|0|OCA60|0|
Teava colt mahon (3 camere)|3 camere|toate|mahon|0|0|0|0|0|0|8
OT60M|0|50|0|
Teava colt stejar (3 camere)|3 camere|toate|stejar|0|0|0|0|0|0|8
OT60S|0|50|0|
Teava colt (4 camere)|4 camere cu garnitura|toate|alb|0|0|0|0|0|0|8
OT60|0|50|0|OCA60|0|OCA60|0|
Teava colt (5 camere)|5 camere cu garnitura|toate|alb|0|0|0|0|0|0|8
OT70|0|50|0|OCA70|0|OCA70|0|
Teava colt mahon (5 camere)|5 camere|toate|mahon|0|0|0|0|0|0|8
OT70M|0|50|0|
Teava colt stejar (5 camere)|5 camere|toate|stejar|0|0|0|0|0|0|8
OT70S|0|50|0|
Prag|toate|toate|toate|0|0|0|0|0|0|10
prg|0|cpcprg|2|
Bandou cu perie|toate|toate|toate|0|0|0|0|0|0|11
bnd|0|
